package model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Person {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long pk;
	private String name;
	private String surname;
	private String fiscalCode;

	public Person(Long pk, String name, String surname) {
		super();
		this.pk = pk;
		this.name = name;
		this.surname = surname;
	}

	public Person() {
		super();
	}

	public Long getPk() {
		return pk;
	}

	public void setPk(Long pk) {
		this.pk = pk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	@Override
	public String toString() {
		return String.format("Person [pk=%s, name=%s, surname=%s]", pk, name, surname);
	}

}
