package controllers;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;

import dao.BaseDao;
import model.Person;

@Stateless
public class PersonController {

	@Inject
	BaseDao<Person> baseDao;

	/**
	 * Questo metodo mi permette di salvare l'oggetto all'interno del mio database
	 * 
	 * @param person
	 * @return
	 */
	public Person store(Person person) throws Exception {
		if (person.getPk() == null) {
			return baseDao.insert(person);
		} else {
			return baseDao.update(person);
		}
	}

	public Person findByPk(Long pk) {
		return baseDao.findByPk(Person.class, pk);
	}

	public List<Person> getAll() {
		return baseDao.getAll(Person.class);
	}

	public Person findByFiscalCode(String fiscalCode) throws NoResultException {
		return null;
	}

}
