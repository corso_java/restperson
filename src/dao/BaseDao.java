package dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

@Stateless
public class BaseDao<T> {

	@PersistenceContext
	EntityManager em;

	/**
	 * Questo metodo permette di salvare un oggetto all'interno del mio database
	 * @param entity
	 * @return
	 */
	public T insert(T entity) {
		em.persist(entity);
		return entity;
	}
	
	public T update(T entity) {
		em.merge(entity);
		return entity;
	}

	public void delete(T entity) {
		em.remove(entity);
	}

	public T findByPk(Class aClass, Long pk) throws NoResultException {
		return (T) em.find(aClass, pk);
	}

	public List<T> getAll(Class aClass) {
		Query query = em.createQuery("Select u From " + aClass.getSimpleName() + " u");
		return query.getResultList();
	}

	public List<T> listResultQuery(Class aClass, String query) {
		TypedQuery<T> result = em.createQuery(query, aClass);
		return result.getResultList();
	}

	public T singleResultQuery(Class aClass, String query, Object... values) {
		try {
			TypedQuery<T> result = em.createQuery(query, aClass);
			for (int i = 0; i < values.length; i++) {
				int position = i + 1;
				result.setParameter(position, values[i]);
			}
			return result.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}
}
