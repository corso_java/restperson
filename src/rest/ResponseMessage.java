package rest;

public class ResponseMessage {

	private int id;
	private String UUID;
	private String message;

	public ResponseMessage(int id, String uUID, String message) {
		super();
		this.id = id;
		UUID = uUID;
		this.message = message;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUUID() {
		return UUID;
	}

	public void setUUID(String uUID) {
		UUID = uUID;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
