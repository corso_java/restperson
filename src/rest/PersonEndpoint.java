package rest;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import controllers.PersonController;
import model.Person;

@Path("/persons")
@Produces("application/json")
@Consumes("application/json")
public class PersonEndpoint {

	@Inject
	PersonController personController;

	@POST
	public Response create(final Person person) {
		try {
			personController.store(person);
			return Response.ok(person).build();
		} catch (Exception e) {
			return Response.serverError().entity(new ResponseMessage(1, UUID.randomUUID().toString(), e.getMessage())).build();
		}

	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	public Response findById(@PathParam("id") final Long id) {
		// TODO: retrieve the person
		Person person = personController.findByPk(id);
		if (person == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(person).build();
	}

	@GET
	public List<Person> listAll(@QueryParam("start") final Integer startPosition,
			@QueryParam("max") final Integer maxResult) {
		// TODO: retrieve the persons
		final List<Person> people = personController.getAll();
		return people;
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	public Response update(@PathParam("id") Long id, final Person person) {
		// TODO: process the given person
		return Response.noContent().build();
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteById(@PathParam("id") final Long id) {
		// TODO: process the person matching by the given id
		return Response.noContent().build();
	}

}
