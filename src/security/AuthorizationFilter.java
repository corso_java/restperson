package security;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import com.google.gson.Gson;

import rest.ResponseMessage;

@Provider
@PreMatching
public class AuthorizationFilter implements ContainerRequestFilter {

	@Context
	HttpServletRequest request;
	@Context
	UriInfo info;

	@Override
	public void filter(ContainerRequestContext container) throws IOException {
		info = container.getUriInfo();

		String oauth = container.getHeaderString(HttpHeaders.AUTHORIZATION);
		System.out.println("Do filtering...");
		if (oauth == null || oauth.isEmpty()) {
			System.out.println("The oauth2 is missing...");
			abort(container);
			return;
		}

		String token = parseToken(oauth);
		System.out.println(token);
		if (!token.equalsIgnoreCase("pluto")) {
			abort(container);
			return;
		}
	}

	private String parseToken(String oauth) {
		return oauth.substring(7);
	}

	private void abort(ContainerRequestContext container) {
		container.abortWith(Response.status(Status.UNAUTHORIZED)
				.entity(new Gson().toJson(new ResponseMessage(400, UUID.randomUUID().toString(), "Non sei autorizzato ad andare avanti")))
				.build());

	}

}
